package com.Credit.credit.Repository;

import com.Credit.credit.Entity.Credit;
import com.Credit.credit.Entity.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {
}
